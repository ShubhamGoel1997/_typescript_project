/* import React, { useState } from "react";

export type counterType<WP extends Object> = React.FC<
  {
    Count: number;
    incrementCount: () => void;
  } & WP
>;
function withCounter<WP>(WrappedComponent: counterType<WP>) {
  
  return (hocProps: Omit<WP, "Count" | "incrementCount">) => {
    const [Count, setCount] = useState(0);

    const incrementCount = () => {
      setCount((prevCount) => prevCount + 1);
    };
    return (
      <WrappedComponent //== Counter
        {...(hocProps as WP)}
        Count={Count}
        incrementCount={incrementCount}
      />
    );
  };
}

export default withCounter;
 */

import React, { useState } from "react";

export type counterType<WP extends Object> = React.FC<
  {
    Count: number;
    incrementCount: () => void;
  } & WP
>;

function withCounter<WP>(WrappedComponent: counterType<WP>) {
  return (hocProps: Omit<WP, "Count" | "incrementCount">) => {
    const [Count, setCount] = useState(0);

    const incrementCount = () => {
      setCount((prevCount) => prevCount + 1);
    };

    return (
      <WrappedComponent
        {...(hocProps as WP)}
        Count={Count}
        incrementCount={incrementCount}
      />
    );
  };
}

export default withCounter;
