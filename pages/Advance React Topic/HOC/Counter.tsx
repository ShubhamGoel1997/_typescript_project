import React from "react";
import HoverCounter from "./HoverCounter";

import withCounter, { counterType } from "./withCounter";

const Counter: counterType<{ name: string }> = (props) => {
  return (
    <div>
      <button onClick={props.incrementCount}>{props.Count}</button>
      <HoverCounter name="shubham" />
    </div>
  );
};

export default withCounter(Counter);
