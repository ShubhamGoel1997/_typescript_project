import React from "react";
import withCounter, { counterType } from "./withCounter";

const HoverCounter: counterType<{ name: string }> = (props) => {
  return (
    <div>
      <h1 onMouseOver={props.incrementCount}>{props.Count}</h1>
      <h1>{props.name}</h1>
    </div>
  );
};

export default withCounter(HoverCounter);
