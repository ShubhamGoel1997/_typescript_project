import React, { useState } from "react";

const Form: React.FC = () => {
  const [name, setName] = useState("");

  const inputref = React.createRef<HTMLInputElement>();

  const handleSubmit = (e: { preventDefault: () => void }) => {
    e.preventDefault();
    alert(
      "the submitted name is(uncontrolled Component) :" +
        inputref.current?.value
    );
    /* console.log("Controlled submittion" + name); */
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        {/*  <input
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        /> */}

        <input type="text" defaultValue="Hello" ref={inputref} />
        {/*  <label>Name:</label>
        <input type="text" ref={inputref} /> */}

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form;
