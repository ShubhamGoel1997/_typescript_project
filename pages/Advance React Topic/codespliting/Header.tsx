import React from "react";
import Link from "next/link";

function Header() {
  return (
    <div>
      <h1>I am the Header</h1>
      <Link href={"./Counter"}>
        <button>Navigate</button>
      </Link>
    </div>
  );
}

export default Header;
