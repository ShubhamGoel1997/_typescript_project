import React, { Suspense } from "react";
import Loadable from "@loadable/component";
import dynamic from "next/dynamic";

const DynamicComponent = dynamic(() => import("./Counter"));

const index = () => {
  return (
    <div>
      <DynamicComponent />
    </div>
  );
};

export default index;
