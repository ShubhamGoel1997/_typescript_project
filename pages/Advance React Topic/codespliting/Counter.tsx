import React, { useState } from "react";

function Counter() {
  const [Count, setCount] = useState(0);

  const incrementCount = () => {
    setCount((prevCount) => prevCount + 1);
  };
  return (
    <div>
      <button onClick={incrementCount}>{Count}</button>
    </div>
  );
}

export default Counter;
