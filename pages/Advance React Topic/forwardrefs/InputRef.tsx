import React, { forwardRef } from "react";

const InputRef = forwardRef<HTMLInputElement>((props, ref) => {
  return (
    <div>
      <input type="text" ref={ref} />
    </div>
  );
});

export default InputRef;
