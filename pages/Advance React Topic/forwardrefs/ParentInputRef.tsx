import React, { createRef } from "react";
import InputRef from "./InputRef";

const ParentInputRef: React.FC = () => {
  const inputref = createRef<HTMLInputElement>();

  const handleclick = () => {
    inputref.current?.focus();
  };

  return (
    <div>
      <InputRef ref={inputref} />
      <button onClick={handleclick}>click me !! </button>
    </div>
  );
};

export default ParentInputRef;
