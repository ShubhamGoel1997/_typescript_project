import React from "react";

type propType = {
  left: React.ReactNode;
  right: React.ReactNode;
};

const Spiltpane: React.FC<propType> = (props) => {
  return (
    <div>
      <div style={{ float: "left" }}>{props.left}</div>

      <div style={{ float: "right" }}>{props.right}</div>
    </div>
  );
};

export default Spiltpane;
