import React from "react";
import Chat from "./Chat";
import Contacts from "./Contacts";
import Spiltpane from "./Spiltpane";

function Application() {
  return (
    <div>
      <Spiltpane left={<Contacts />} right={<Chat />} />
    </div>
  );
}

export default Application;
