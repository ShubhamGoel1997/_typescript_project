import React from "react";
import FancyBorder from "./FancyBorder";

function WelcomeDialogue() {
  return (
    <FancyBorder color="blue">
      <p>Hello</p>

      <h1>this is composition</h1>
    </FancyBorder>
  );
}

export default WelcomeDialogue;
