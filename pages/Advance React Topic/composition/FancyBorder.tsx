import React from "react";

type propstype = {
  color: string;
};

const FancyBorder: React.FC<propstype> = (props) => {
  return (
    <div className={"FancyBorder FancyBorder-" + props.color}>
      {props.children}
    </div>
  );
};

export default FancyBorder;
