const aob = [{ price: 101 }, { price: 11 }, { price: 40 }];

const forSort = aob.sort((a) => {
  return a.price;
});
console.log(forSort);

const forSlice = aob.slice(0, 2);
console.log(forSlice);

const helloFunction = async () => {
  await fetch();
  console.log("z");
  var a = 5;
  var b = 10;
  var c = a * b;
  return c;
};
const makeFetch = async () => {
  const helloResponse = helloFunction().then((response) => {
    //console.log("response is ", response);
    console.log("e");
  });
  console.log("y");
};

useEffect(() => {
  console.log("1");
  makeFetch();
  console.log("2");
});
//z - e - y --> await
// z - y - e --> fetch
