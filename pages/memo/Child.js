import React, { memo } from "react";

function Child(props) {
  return (
    <div>
      {console.log("child render")}
      <h1>Hello</h1>
      <p>{props.title}</p>
    </div>
  );
}

export default memo(Child);
