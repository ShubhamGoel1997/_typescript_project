import React, { useMemo, useState } from "react";
import Child from "./Child";

function index() {
  const [count, setCount] = useState(0);

  /* const oneArray = ["one", "two", "three"]; */ //rerenders every time and react a new array everytime

  const oneArray = useMemo(() => {
    return ["one", "two", "three"];
  }, []);

  return (
    <div>
      {count}

      <button onClick={() => setCount((val) => val + 1)}>Increment</button>

      <Child title="Using memo and callback" Array={oneArray} />
    </div>
  );
}

export default index;
