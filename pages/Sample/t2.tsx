/*
 *   Copyright (c) 2021 RCUBE PLANET PVT LTD
 *   All rights reserved.
 */

import type { NextPage } from "next";
import { Reducer, useReducer } from "react";

type stateType = {
  name: string;
};

type reducerAction =
  | { type: "update"; payload: { name: string } }
  | { type: "reset"; payload?: undefined };

const Home: NextPage = () => {
  const reducer = (state: stateType, action: reducerAction): stateType => {
    switch (action.type) {
      case "update":
        return { ...state, name: action.payload.name };
      case "reset":
        return { ...state, name: "" };
      default:
        return state;
    }
  };
  //reducer overwrite
  const [state, dispatch] = useReducer<Reducer<stateType, reducerAction>>(
    reducer,
    { name: "" }
  );

  const _fn = {
    onNameUpdate: (e: React.ChangeEvent<HTMLInputElement>) => {
      dispatch({ type: "update", payload: { name: e.target.value } });
    },
    onRest: () => {
      dispatch({ type: "reset" });
    },
  };

  return (
    <div>
      <div>
        <input onChange={_fn.onNameUpdate} value={state.name}></input>
        <button onClick={_fn.onRest}>Reset</button>
      </div>

      {state.name.length === 0 ? (
        <p>Type something</p>
      ) : (
        <p>You have typed - {state.name}</p>
      )}
    </div>
  );
};

export default Home;
