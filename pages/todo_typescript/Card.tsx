import React from "react";

type CardPropType = {
  item?: { name: string };
  index?: number;
  onDelete?: (index: number) => void;
  ListEdit?: (index: number) => void;
};

export const Card: React.FC<CardPropType> = (props) => {
  return (
    <div>
      {/* {display element} */ props.item?.name}
      {
        /* {Make a delete button} */
        <div>
          <button
            onClick={() => {
              props.onDelete?.(props.index!);
            }}
          >
            Delete
          </button>
          <button
            onClick={() => {
              props.ListEdit?.(props.index!);
            }}
          >
            edit
          </button>
        </div>
      }
    </div>
  );
};
export default Card;
