import React from "react";
import react, { useState } from "react";

type EditorPropType = {
  name?: string;
  type?: string;
  index?: number;
  onEdit?: (index: number, obj: { name: string }) => void;
  onAdd?: (obj: { name: string }) => void;
};

export const Editor: React.FC<EditorPropType> = (props) => {
  const [state, setState] = useState({
    name: props.name == undefined ? "" : props.name,
  });

  const _fn1 = {
    save: () => {
      //For editing and adding
      //if type='edit' call onEdit
      //if type='add' call onAdd

      if (props.type == "edit") {
        props.onEdit?.(props.index!, state);
      } else {
        props.onAdd?.(state);
      }
    },
  };
  return (
    <div>
      {/* {display type Add or Edit} */ props.type}
      <input
        value={state.name}
        onChange={(e) => {
          setState({ ...state, name: e.target.value });
        }}
      />

      {
        /* {make a button to save} */
        <button onClick={_fn1.save}>Save data</button>
      }
    </div>
  );
};
export default Editor;
