import React, { useState } from "react";
import Editor from "./Editor";
import Card from "./Card";

type listType = {
  mode: "edit" | "view";
  name: string;
}[];

export const Index: React.FC = () => {
  const [list, setList] = useState<listType>([]);
  const _fn = {
    add: (newItem: { name: string }) => {
      //for adding elements
      const addList = [...list];
      addList.push({
        ...newItem,
        mode: "view",
      });
      setList(addList);
    },
    edit: (nameIndex: number, updatedName: { name: string }) => {
      //for editing element

      const editedElement: listType = list.map((item, i) => {
        if (i === nameIndex) {
          return {
            ...updatedName,
            mode: "view",
          };
        }
        return item;
      });
      console.log("edit is called", editedElement);
      setList(editedElement);
    },

    ListEdit: (listIndex: number) => {
      const newList: listType = list.map((item, i) => {
        if (listIndex == i) {
          return {
            ...item,

            mode: "edit",
          };
        } else {
          return item;
        }
      });

      //console.log(newList)
      setList(newList);
    },

    delete: (itemIndex: number) => {
      // for deleting element
      const updatedList: listType = list.filter(
        (val, index) => index !== itemIndex
      );
      setList(updatedList);
    },
  };
  return (
    <div>
      <div>
        <h1>Add a new user</h1>
        <Editor type="add" onAdd={_fn.add} />
      </div>
      <div>
        {list.map((item, i) => {
          return (
            <div key={i}>
              {item.mode == "edit" ? (
                <Editor
                  type="edit"
                  index={i}
                  name={item.name}
                  onEdit={_fn.edit}
                />
              ) : (
                <Card
                  item={item}
                  index={i}
                  onDelete={_fn.delete}
                  ListEdit={_fn.ListEdit}
                />
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default Index;
