import React, { useRef, useState, useEffect } from "react";
import OutsideClickHandler from "react-outside-click-handler";
import styles from "./down.module.scss";
import AbortController from "abort-controller";

type apiData = {
  airport_name: string;
  airport_code: string;
  city_code: string;
  city_name: string;
  country_code: string;
  country_name: string;
}[];

const Dropdown: React.FC = () => {
  const [dataOptions, setDataOptions] = useState<apiData>([]);
  const [isOpen, setIsOpen] = useState<Boolean>(false);
  const [input, setInput] = useState<string>("");
  const [selectedOption, setSelectedOption] = useState<string | apiData | null>(
    ""
  );
  const inputRef = useRef<HTMLInputElement>(null);

  function goFocus() {
    inputRef.current?.focus();
  }

  useEffect(() => {
    console.log("useEffect called!!");
    if (isOpen) {
      goFocus();
    }
  }, [isOpen]);

  useEffect(() => {
    console.log("fetch called!!");

    const controller = new AbortController();
    const url = "http://pre.triphexa.com/web-api/search_airport";

    const options = {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },

      body: JSON.stringify({
        //"{"search : input"}"
        search: input,
      }),
      signal: controller.signal,
    };

    fetch(url, options)
      .then((res) => {
        console.log("response data", res);
        return res.json();
      })
      .then((data) => {
        console.log(data.data);
        setDataOptions(data.data);
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {
      controller.abort();
      console.log("request cancelled!!");
    };
  }, [input]);

  return (
    <div className={styles.dropdown}>
      <h1>Dropdown</h1>
      <OutsideClickHandler
        onOutsideClick={(e) => {
          setIsOpen(false);
        }}
      >
        {!isOpen ? (
          <div className={styles.span}>
            <span
              onClick={(e) => {
                setIsOpen(true);
                setInput("");
              }}
            >
              {selectedOption ? selectedOption : "select"}
            </span>

            <div>
              <button
                className={styles.reset_btn}
                onClick={(e) => {
                  setSelectedOption("");
                }}
              >
                Reset
              </button>
            </div>
          </div>
        ) : (
          <input
            ref={inputRef}
            className={styles.dropdown_btn}
            value={input}
            placeholder="search..."
            onChange={(e) => setInput(e.target.value)}
          />
        )}

        {isOpen && (
          <div className={styles.dropdown_content}>
            {console.log(dataOptions)}

            {dataOptions.map((item, index) => (
              <div
                className={styles.dropdown_item}
                key={index}
                onClick={(e) => {
                  setSelectedOption(
                    `${item.city_name}-${item.city_code},${item.country_name}`
                  );
                  setIsOpen(false);
                }}
              >
                <div style={{ float: "left" }}>{item.city_name}</div>
                <br />

                <div style={{ float: "left", marginTop: 20 }}>
                  {item.airport_name}
                </div>
                <div style={{ float: "right", marginTop: -20 }}>
                  {item.city_code}
                </div>
                <br />

                <div style={{ float: "right" }}>{item.country_name}</div>
              </div>
            ))}
          </div>
        )}
      </OutsideClickHandler>
    </div>
  );
};

export default Dropdown;
