import { NextPage } from "next";

type dataType = {
  userId: number;
  id: number;
  title: string;
};

type propsType = {
  data: dataType[];
};

const Comments: NextPage<propsType> = ({ data }) => {
  return (
    <div>
      <h1>All Comments</h1>

      <div>
        {data.map((item, index) => (
          <div key={index}>
            {item.id} <span>{item.title}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

Comments.getInitialProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/albums");
  const data = await res.json();

  return {
    data,
  };
};

export default Comments;
