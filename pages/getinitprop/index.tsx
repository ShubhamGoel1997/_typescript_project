import { NextPage } from "next";
import React from "react";

type dataType = {
  id: number;
  url: string;
  title: string;
}[];

type propType = {
  data: dataType;
};

const Photos: NextPage<propType> = ({ data }) => {
  return (
    <div>
      {data.map((item, index) => (
        <div key={index}>
          {item.id} <br />
          {item.title} <br />
          {item.url}
        </div>
      ))}
    </div>
  );
};

Photos.getInitialProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/photos");
  const data = await res.json();

  return {
    data,
  };
};

export default Photos;
