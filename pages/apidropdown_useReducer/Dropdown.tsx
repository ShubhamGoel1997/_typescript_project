import React, { useRef, useReducer, useEffect } from "react";
import OutsideClickHandler from "react-outside-click-handler";
import styles from "./down.module.css";
import AbortController from "abort-controller";

type ACTIONS =
  | { type: "setDataOptions"; payload: { info: apiData } }
  | { type: "setInput"; payload: { inp: string } }
  | { type: "setInputForSpan" }
  | { type: "isOpenForOutsideClick" }
  | { type: "isOpenForSpan" }
  | { type: "isOpenForDropdownItem" }
  | { type: "selectOptionForReset" }
  | { type: "selectOptionForDropdownItem"; payload: { item: string } };

type apiData = {
  airport_name: string;
  airport_code: string;
  city_code: string;
  city_name: string;
  country_code: string;
  country_name: string;
}[];

type reducertype = {
  dataOptions: apiData;
  isOpen: Boolean;
  input: string;
  selectedOption: string | null | apiData;
};

const reducer = (state: reducertype, action: ACTIONS): reducertype => {
  switch (action.type) {
    case "setDataOptions":
      return {
        ...state,
        dataOptions: action.payload.info,
      };
    case "setInput":
      return {
        ...state,
        input: action.payload.inp,
      };
    case "setInputForSpan":
      return {
        ...state,
        input: "",
      };
    case "isOpenForOutsideClick":
      return {
        ...state,
        isOpen: false,
      };
    case "isOpenForSpan":
      return {
        ...state,
        isOpen: true,
      };
    case "isOpenForDropdownItem":
      return {
        ...state,
        isOpen: false,
      };
    case "selectOptionForReset":
      return {
        ...state,
        selectedOption: "",
      };
    case "selectOptionForDropdownItem":
      return {
        ...state,
        selectedOption: action.payload.item,
      };
  }
};

const initialValues = {
  dataOptions: [],
  isOpen: false,
  selectedOption: null,
  input: "",
};

const Dropdown: React.FC = () => {
  const inputRef = useRef<HTMLInputElement>(null);

  const [actionOnOptions, dispatch] = useReducer(reducer, initialValues);

  function goFocus() {
    inputRef.current?.focus();
  }

  useEffect(() => {
    console.log("useEffect called!!");
    if (actionOnOptions.isOpen) {
      goFocus();
    }
  }, [actionOnOptions.isOpen]);

  useEffect(() => {
    console.log("fetch called!!");

    const controller = new AbortController();
    const url = "http://pre.triphexa.com/web-api/search_airport";

    const options = {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },

      body: JSON.stringify({
        search: actionOnOptions.input,
      }),
      signal: controller.signal,
    };

    fetch(url, options)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data.data);
        dispatch({ type: "setDataOptions", payload: { info: data.data } });
      })
      .catch((err) => {
        console.log(err);
      });
    return () => {
      controller.abort();
      console.log("request cancelled!!");
    };
  }, [actionOnOptions.input]);

  return (
    <div className={styles.dropdown}>
      <h1>Dropdown</h1>
      <OutsideClickHandler
        onOutsideClick={(e) => {
          dispatch({ type: "isOpenForOutsideClick" });
        }}
      >
        {!actionOnOptions.isOpen ? (
          <div className={styles.span}>
            <span
              onClick={(e) => {
                dispatch({ type: "isOpenForSpan" });
                dispatch({ type: "setInputForSpan" });
              }}
            >
              {actionOnOptions ? actionOnOptions.selectedOption : "select"}
            </span>

            <div>
              <button
                className={styles.reset_btn}
                onClick={() => {
                  dispatch({ type: "selectOptionForReset" });
                }}
              >
                Reset
              </button>
            </div>
          </div>
        ) : (
          <input
            ref={inputRef}
            className={styles.dropdown_btn}
            value={actionOnOptions.input}
            placeholder="search..."
            onChange={(e) =>
              dispatch({ type: "setInput", payload: { inp: e.target.value } })
            }
          />
        )}

        {actionOnOptions.isOpen && (
          <div className={styles.dropdown_content}>
            {console.log(actionOnOptions.dataOptions)}

            {actionOnOptions.dataOptions.map((item, index) => (
              <div
                className={styles.dropdown_item}
                key={index}
                onClick={(e) => {
                  dispatch({
                    type: "selectOptionForDropdownItem",
                    payload: {
                      item: `${item.city_name}-${item.city_code},${item.country_name}`,
                    },
                  });
                  dispatch({ type: "isOpenForDropdownItem" });
                }}
              >
                <div style={{ float: "left" }}>{item.city_name}</div>
                <br />

                <div style={{ float: "left", marginTop: 20 }}>
                  {item.airport_name}
                </div>
                <div style={{ float: "right", marginTop: -20 }}>
                  {item.city_code}
                </div>
                <br />

                <div style={{ float: "right" }}>{item.country_name}</div>
              </div>
            ))}
          </div>
        )}
      </OutsideClickHandler>
    </div>
  );
};

export default Dropdown;
