import { GetStaticProps, InferGetStaticPropsType } from "next";

type albumData = {
  userId: number;
  id: number;
  title: string;
};

type PropsData = {
  data: albumData[];
};

export const getStaticProps: GetStaticProps<PropsData> = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/albums");
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};
const Albums = (props: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <div>
      <h1>Albums</h1>
      {props.data.map((item, index) => (
        <div key={index}>
          <p> {`id is : ${item.id}`}</p>
          <p>{`User Id is : ${item.userId}`}</p>
        </div>
      ))}
    </div>
  );
};
export default Albums;
