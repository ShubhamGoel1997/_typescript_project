import {
  GetStaticPaths,
  GetStaticPathsContext,
  GetStaticProps,
  GetStaticPropsContext,
  InferGetStaticPropsType,
} from "next";

type dataType = {
  userId: number;
  id: number;
  title: string;
};

type Propstype = {
  data: dataType;
};

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/albums");
  const data: dataType[] = await res.json();

  const paths = data.map((data) => {
    return {
      params: {
        id: data.id.toString(),
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Propstype> = async (
  context: GetStaticPropsContext
) => {
  const id = context.params?.id;

  const res = await fetch(`https://jsonplaceholder.typicode.com/albums/${id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};

const AlbumDetails = (
  props: InferGetStaticPropsType<typeof getStaticProps>
) => {
  return (
    <div>
      <h1>{`Title is : ${props.data.title}`}</h1>
    </div>
  );
};
export default AlbumDetails;
