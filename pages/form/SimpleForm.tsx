import { useFormik } from "formik";
import styles from "./form.module.css";
import React from "react";
import { ChromePicker } from "react-color";
import * as Yup from "yup";
type initialType = {
  email: string;
  password: string;
  color: string;
  location: string;
  Gender: string;
  Account: string;
};
const initialValues = {
  email: "",
  password: "",
  color: "",
  location: "",
  Gender: "",
  Account: "",
};

const onSubmit = (values: initialType) => {
  console.log("Data fetched", values);
};

const validationSchema = Yup.object({
  email: Yup.string().email("Invalid email format").required("Required!!"),
  password: Yup.string().required("Required!!"),
  color: Yup.string().required("Required"),
  location: Yup.string().required("REQuIRED"),
  Gender: Yup.string().required("REQuIRED"),
  Account: Yup.string().required("REQuIRED"),
});

function SimpleForm() {
  const formik = useFormik<initialType>({
    initialValues,
    validationSchema,
    onSubmit,
  });

  console.log("form values", formik.values);
  console.log("form error", formik.errors);
  console.log("form touched", formik.touched);

  return (
    <div className={styles.main}>
      <form onSubmit={formik.handleSubmit}>
        <h1>User Form</h1>
        <div className={styles.form_control}>
          <label className={styles.label} htmlFor="email">
            Email:
          </label>
          <input
            className={styles.input}
            type="email"
            id="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.email && formik.errors.email ? (
            <div className={styles.error}>{formik.errors.email}</div>
          ) : null}
        </div>
        <div className={styles.form_control}>
          <label className={styles.label} htmlFor="password">
            Password:
          </label>
          <input
            className={styles.input}
            type="password"
            id="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password && formik.errors.password ? (
            <div className={styles.error}>{formik.errors.password}</div>
          ) : null}
        </div>

        <div className={styles.form_control}>
          <label className={styles.label} htmlFor="dropdown">
            Social ACcount :
          </label>
          <select
            name="Account"
            id="dropdown"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          >
            <option value="0">Select Account </option>
            <option value="Facebook">Facebook</option>
            <option value="Twitter">Twitter</option>
            <option value="Instagram">Instagram</option>
          </select>
          {formik.touched.Account && formik.errors.Account ? (
            <div className={styles.error}>{formik.errors.Account}</div>
          ) : null}
        </div>

        <div className={styles.form_control}>
          <label className={styles.label} htmlFor="checkbox">
            Location :
          </label>
          <input
            type="checkbox"
            id="checkbox"
            name="location"
            value="Delhi"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Delhi
          <input
            type="checkbox"
            id="checkbox"
            name="location"
            value="Lucknow"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Lucknow
          <input
            type="checkbox"
            id="checkbox"
            name="location"
            value="Rajasthan"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Rajasthan
          {formik.touched.location && formik.errors.location ? (
            <div className={styles.error}>{formik.errors.location}</div>
          ) : null}
        </div>

        <div className={styles.form_control}>
          <label className={styles.label} htmlFor="checkbox">
            Gender :
          </label>
          <input
            type="radio"
            id="radio"
            name="Gender"
            value="Male"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Male
          <input
            type="radio"
            id="radio"
            name="Gender"
            value="Female"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Female
          <input
            type="radio"
            id="radio"
            name="Gender"
            value="Other"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          Others
          {formik.touched.Gender && formik.errors.Gender ? (
            <div className={styles.error}>{formik.errors.Gender}</div>
          ) : null}
        </div>

        <div className={styles.form_control}>
          <label className={styles.label}>Pick a Color:</label>

          <ChromePicker
            color={formik.values.color}
            onChange={(updatedColor) =>
              formik.setFieldValue("color", updatedColor.hex)
            }
          />
          <input
            type="text"
            name="color"
            id="name"
            value={formik.values.color}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />

          {formik.touched.color && formik.errors.color ? (
            <div className={styles.error}>{formik.errors.color}</div>
          ) : null}
        </div>

        <button className={styles.btn} type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}

export default SimpleForm;
