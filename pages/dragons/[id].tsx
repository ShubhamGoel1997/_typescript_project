import {
  GetStaticPaths,
  GetStaticProps,
  GetStaticPropsContext,
  InferGetStaticPropsType,
} from "next";

type propType = {
  name: string;
  id: number;
  email: string;
  website: string;
  address: { city: string };
};

type usertype = {
  data: propType;
};
export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/users");
  const data: propType[] = await res.json();

  const paths = data.map((data) => {
    return {
      params: { id: data.id.toString() },
    };
  });
  console.log(paths);

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<usertype> = async (
  context: GetStaticPropsContext
) => {
  //context is an object containing following key - params,locale,preview,previewData,locales,defaultLocale
  const id = context.params?.id;

  const res = await fetch("https://jsonplaceholder.typicode.com/users/" + id);
  const data = await res.json();

  return {
    props: { data },
  };
};

const Details = (props: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <div>
      <h1>Details Page</h1>
      <h1>{props.data.name}</h1>
      <p>{props.data.email}</p>
      <p>{props.data.website}</p>
      <p>{props.data.address.city}</p>
    </div>
  );
};

export default Details;
