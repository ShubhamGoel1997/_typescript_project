/* import styles from "../../styles/dragons.module.css"; */
import Link from "next/link";
import { GetStaticProps, InferGetStaticPropsType } from "next";

type dataType = {
  data: {
    id: number;
    name: string;
  }[];
};
export const getStaticProps: GetStaticProps<dataType> = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/users");
  const data = await res.json();

  return {
    props: { data },
  };
};

const Dragons = (props: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <div>
      <h1>All Dragons</h1>
      {props.data.map((dragon) => (
        <Link href={"/dragons/" + dragon.id} key={dragon.id}>
          <a /* className={styles.single} */>
            <h1>{dragon.name}</h1>
          </a>
        </Link>
      ))}
    </div>
  );
};

export default Dragons;
