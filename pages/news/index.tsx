import { GetServerSideProps, InferGetServerSidePropsType } from "next";

type datatype = {
  data: {
    id: number;
    title: string;
    category: string;
  }[];
};

export const getServerSideProps: GetServerSideProps<datatype> = async () => {
  const res = await fetch("http://localhost:4000/news");
  console.log(res);
  const data = await res.json();

  return {
    props: { data },
  };
};

const NewsArticleList = ({
  data,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <div>
      <h1>List of articles</h1>
      {data.map((article) => {
        return (
          <div key={article.id}>
            <h2>
              {article.id} {article.title} | {article.category}
            </h2>
          </div>
        );
      })}
    </div>
  );
};

export default NewsArticleList;
