import {
  GetServerSideProps,
  GetServerSidePropsContext,
  InferGetServerSidePropsType,
} from "next";

type datatype = {
  id: number;
  title: string;
  description: string;
};

type Propstype = {
  data: datatype[];
  category: string;
};

export const getServerSideProps: GetServerSideProps<
  Propstype,
  { category: string }
> = async (context) => {
  // const { params } = context;
  // const { category } = params;
  const category = context.params?.category ?? "";

  const res = await fetch(`http://localhost:4000/news?category=${category}`);
  const data = await res.json();

  return {
    props: {
      data,
      category,
    },
  };
};

const ArticleListByCategory = ({
  data,
  category,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <div>
      <h1>
        Show me News for Category <i>{category}</i>
      </h1>

      {data.map((article) => {
        return (
          <div key={article.id}>
            <h2>
              {article.id} {article.title}
            </h2>
            <p>{article.description}</p>
            <hr />
          </div>
        );
      })}
    </div>
  );
};

export default ArticleListByCategory;
