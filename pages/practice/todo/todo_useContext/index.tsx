import React, { useState } from "react";
import Card from "./Card";
import Editor from "./Editor";

export const EditorContext = React.createContext(null as any);
export const CardContext = React.createContext(null as any);

type listType = {
  name: string;
  mode: "view" | "edit";
}[];

const index = () => {
  const [list, setList] = useState<listType>([]);

  const fn = {
    add: (newName: { name: string }) => {
      const newItem = [...list];
      newItem.push({
        ...newName,
        mode: "view",
      });
      setList(newItem);
    },
    edit: (clickedIndex: number, newName: { name: string }) => {
      const editedItem: listType = list.map((item, i) => {
        if (i === clickedIndex) {
          return {
            ...newName,
            mode: "view",
          };
        }
        return item;
      });
      setList(editedItem);
    },
    delete: (clickedIndex: number) => {
      const updatedList = list.filter((item, i) => i !== clickedIndex);
      setList(updatedList);
    },
    swtch2Edit: (clickedIndex: number) => {
      const swtchItem2EditMode: listType = list.map((item, i) => {
        if (i === clickedIndex) {
          return {
            ...item,
            mode: "edit",
          };
        }
        return item;
      });
      setList(swtchItem2EditMode);
    },
  };

  return (
    <div>
      <div>
        <h1>Add user </h1>
        <EditorContext.Provider value={{ type: "add", onAdd: fn.add }}>
          <Editor />
        </EditorContext.Provider>
      </div>

      <div>
        {list.map((item, i) => (
          <div key={i}>
            {item.mode == "edit" ? (
              <EditorContext.Provider
                value={{
                  onEdit: fn.edit,
                  name: item.name,
                  index: i,
                  type: "edit",
                }}
              >
                <Editor />
              </EditorContext.Provider>
            ) : (
              <CardContext.Provider
                value={{
                  item: item,
                  index: i,
                  onDelete: fn.delete,
                  Onswtch2Edit: fn.swtch2Edit,
                }}
              >
                <Card />
              </CardContext.Provider>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default index;
