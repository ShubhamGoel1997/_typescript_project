import React, { useContext } from "react";
import { CardContext } from "./index";

type cardPropsType = {
  item?: { name: string };
  index?: number;
  onDelete?: (index: number) => void;
  Onswtch2Edit?: (index: number) => void;
};

const Card = () => {
  const CardProps = useContext<cardPropsType>(CardContext);

  return (
    <div>
      {CardProps.item?.name}

      <button onClick={() => CardProps.onDelete?.(CardProps.index!)}>
        Delete
      </button>
      <button onClick={() => CardProps.Onswtch2Edit?.(CardProps.index!)}>
        Swtch2Edit
      </button>
    </div>
  );
};

export default Card;
