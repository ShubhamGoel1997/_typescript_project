import React, { useContext, useState } from "react";
import { EditorContext } from "./index";

type EditorPropType = {
  name: string;
  onEdit: (index: number, obj: { name: string }) => void;
  onAdd: (obj: { name: string }) => void;
  type: string;
  index: number;
};

const Editor: React.FC = () => {
  const EditorProps = useContext<EditorPropType>(EditorContext);

  const [state, setState] = useState({
    name: EditorProps.name == undefined ? "" : EditorProps.name,
  });

  const fn1 = {
    save: () => {
      if (EditorProps.type == "edit") {
        EditorProps.onEdit(EditorProps.index, state);
      } else {
        EditorProps.onAdd(state);
      }
    },
  };

  return (
    <div>
      {EditorProps.type}

      <input
        type="text"
        value={state.name}
        onChange={(e) => setState({ ...state, name: e.target.value })}
      />

      <button onClick={fn1.save}>save</button>
    </div>
  );
};

export default Editor;
