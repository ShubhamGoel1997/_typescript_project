import React, { useReducer } from "react";
import Card from "./Card";
import Editor from "./Editor";

type Actions =
  | { type: "add"; payload: { name: string } }
  | { type: "edit"; payload: { index: number; name: string } }
  | { type: "delete"; payload: { index: number } }
  | { type: "swtch2Edit"; payload: { index: number } };

type listType = {
  name: string;
  mode: "view" | "edit";
}[];

const reducer = (list: listType, action: Actions): listType => {
  switch (action.type) {
    case "add":
      const newName = [...list];
      newName.push({
        ...action.payload,
        mode: "view",
      });
      return newName;

    case "edit":
      const editedElement: listType = list.map((item, i) => {
        if (i === action.payload.index) {
          return {
            ...action.payload,
            mode: "view",
          };
        }
        return item;
      });
      return editedElement;

    case "delete":
      const filteredList = list.filter((item, i) => i !== action.payload.index);
      return filteredList;

    case "swtch2Edit":
      const item2swtch: listType = list.map((item, i) => {
        if (i === action.payload.index) {
          return {
            ...item,
            mode: "edit",
          };
        }

        return item;
      });
      return item2swtch;

    default:
      return list;
  }
};

const index: React.FC = () => {
  const [objData, dispatch] = useReducer(reducer, []);

  const fn = {
    add: (newName: { name: string }) => {
      dispatch({ type: "add", payload: { name: newName.name } });
    },
    edit: (clickedIndex: number, newName: { name: string }) => {
      dispatch({
        type: "edit",
        payload: { index: clickedIndex, name: newName.name },
      });
    },
    delete: (clickedIndex: number) => {
      dispatch({ type: "delete", payload: { index: clickedIndex } });
    },
    swtch2Edit: (clickedIndex: number) => {
      dispatch({ type: "swtch2Edit", payload: { index: clickedIndex } });
    },
  };

  return (
    <div>
      <div>
        <h1>Add user</h1>
        <Editor type="add" onAdd={fn.add} />
      </div>
      <div>
        {objData.map((item, i) => (
          <div key={i}>
            {item.mode == "edit" ? (
              <Editor type="edit" index={i} name={item.name} onEdit={fn.edit} />
            ) : (
              <Card
                item={item}
                index={i}
                onDelete={fn.delete}
                onSwtch2Edit={fn.swtch2Edit}
              />
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default index;
