import React, { useReducer } from "react";

type EditorPropType = {
  type?: string;
  name?: string;
  index?: number;
  onAdd?: (obj: { name: string }) => void;
  onEdit?: (index: number, obj: { name: string }) => void;
};

const Editor: React.FC<EditorPropType> = (props) => {
  const initialValue = {
    name: props.name == undefined ? "" : props.name,
  };

  type state = {
    name: string;
  };

  type Actions = { type: "setState"; payload: { name: string } };

  const reducer = (state: state, action: Actions) => {
    switch (action.type) {
      case "setState":
        return { ...state, name: action.payload.name };
    }
  };

  const [state, dispatch] = useReducer(reducer, initialValue);

  const fn1 = {
    save: () => {
      if (props.type === "edit") {
        props.onEdit?.(props.index!, state);
      } else {
        props.onAdd?.(state);
      }
    },

    setState: (e: React.ChangeEvent<HTMLInputElement>) => {
      dispatch({ type: "setState", payload: { name: e.target.value } });
    },
  };
  return (
    <div>
      {props.type}

      <input type="text" value={state.name} onChange={fn1.setState} />

      <button onClick={fn1.save}>Save</button>
    </div>
  );
};

export default Editor;
