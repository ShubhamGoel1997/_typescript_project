import React from "react";

type cardPropType = {
  index?: number;
  item?: { name: string };
  onDelete?: (index: number) => void;
  onSwtch2Edit?: (index: number) => void;
};

const Card: React.FC<cardPropType> = (props) => {
  return (
    <div>
      {props.item?.name}

      <button onClick={() => props.onDelete?.(props.index!)}>delete</button>
      <button onClick={() => props.onSwtch2Edit?.(props.index!)}>
        swtch2Edit
      </button>
    </div>
  );
};

export default Card;
