import React from "react";

type CardPropType = {
  onSwtch2Edit?: (index: number) => void;
  onDelete?: (index: number) => void;
  item?: { name: string };
  index?: number;
};

const Card: React.FC<CardPropType> = (props) => {
  return (
    <div>
      {props.item?.name}

      <div>
        <button onClick={() => props.onDelete?.(props.index!)}>delete</button>
        <button onClick={() => props.onSwtch2Edit?.(props.index!)}>Edit</button>
      </div>
    </div>
  );
};

export default Card;
