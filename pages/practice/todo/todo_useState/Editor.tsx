import React, { useState } from "react";

type propsType = {
  type?: string;
  name?: string;
  index?: number;
  onadd?: (obj: { name: string }) => void;
  onEdit?: (index: number, obj: { name: string }) => void;
};

const Editor: React.FC<propsType> = (props) => {
  const [state, setState] = useState({
    name: props.name == undefined ? "" : props.name,
  });

  const fn1 = {
    save: () => {
      if (props.type == "edit") {
        props.onEdit?.(props.index!, state);
      } else {
        props.onadd?.(state);
      }
    },
  };

  return (
    <div>
      {props.type}
      <input
        value={state.name}
        onChange={(e) => setState({ ...state, name: e.target.value })}
      />

      {<button onClick={fn1.save}>save</button>}
    </div>
  );
};

export default Editor;
