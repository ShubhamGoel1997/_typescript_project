import React, { useState } from "react";
import Editor from "./Editor";
import Card from "./Card";
type listType = {
  mode: "view" | "edit";
  name: string;
}[];

const Index: React.FC = () => {
  const [list, setList] = useState<listType>([]);

  const fn = {
    add: (newName: { name: string }) => {
      const addElement = [...list];
      addElement.push({
        ...newName,
        mode: "view",
      });
      setList(addElement);
    },
    edit: (clickedIndex: number, obj: { name: string }) => {
      const editedElement: listType = list.map((item, i) => {
        if (i === clickedIndex) {
          return {
            ...obj,
            mode: "view",
          };
        }
        return item;
      });
      setList(editedElement);
    },
    delete: (clickedIndex: number) => {
      const newlist = list.filter((val, i) => i !== clickedIndex);
      setList(newlist);
    },
    swtch2Edit: (clickedIndex: number) => {
      const listItem: listType = list.map((item, i) => {
        if (i == clickedIndex) {
          return {
            ...item,
            mode: "edit",
          };
        }
        return item;
      });
      setList(listItem);
    },
  };

  return (
    <div>
      <div>
        <h1>Add a user</h1>
        <Editor type="add" onadd={fn.add} />
      </div>
      <div>
        {list.map((item, i) => (
          <div key={i}>
            {item.mode == "edit" ? (
              <Editor type="edit" index={i} name={item.name} onEdit={fn.edit} />
            ) : (
              <Card
                index={i}
                item={item}
                onSwtch2Edit={fn.swtch2Edit}
                onDelete={fn.delete}
              />
            )}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Index;
