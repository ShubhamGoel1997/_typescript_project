import { NextPage } from "next";

type datatype = {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
};
type propsType = {
  data: datatype[];
};

const UserData: NextPage<propsType> = ({ data }) => {
  return (
    <div>
      {data.map((item, index) => (
        <div key={index}>
          <div>
            {`The PostId is : ${item.postId}`}
            <br />
            {`The name is : ${item.name}`}
            <br />
            {`The email is : ${item.email}`}
          </div>
        </div>
      ))}
    </div>
  );
};

UserData.getInitialProps = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/comments");
  const data = await res.json();

  return {
    data,
  };
};

export default UserData;
