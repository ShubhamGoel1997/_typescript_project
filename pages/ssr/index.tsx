import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import React from "react";

type dataType = {
  albumId: number;
  id: number;
  title: string;
  url: string;
}[];

type propType = {
  data: dataType;
};

export const getServerSideProps: GetServerSideProps<propType> = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/photos");
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};

const index = (
  props: InferGetServerSidePropsType<typeof getServerSideProps>
) => {
  return (
    <div>
      {props.data.map((item, index) => {
        return (
          <div key={index}>
            {item.id} <br />
            {item.url}
          </div>
        );
      })}
    </div>
  );
};

export default index;
