import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import React from "react";

type dataType = {
  title: string;
  albumId: number;
  url: string;
}[];

type propType = {
  data: dataType;
};

export const getServerSideProps: GetServerSideProps<propType> = async (
  context
) => {
  const id = context.params?.id;
  const res = await fetch(
    `https://jsonplaceholder.typicode.com/photos?id=${id}`
  );
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};

const Album = (
  props: InferGetServerSidePropsType<typeof getServerSideProps>
) => {
  return (
    <div>
      {props.data.map((item, index) => (
        <div key={index}>
          {item.url} <br />
          {item.title} <br />
        </div>
      ))}
    </div>
  );
};

export default Album;
