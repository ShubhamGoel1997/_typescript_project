import React, { useState, useContext } from "react";
import Editor from "./Editor";
import Card from "./Card";

export const EditorContext = React.createContext(null as any);
export const CardContext = React.createContext(null as any);

type todo = {
  name: string;
  mode: "edit" | "view";
}[];

const Index: React.FC = () => {
  const [list, setList] = useState<todo>([]);

  const _fn = {
    add: (newItem: { name: string }) => {
      //for adding elements
      const addList: todo = [...list];
      addList.push({
        ...newItem,
        mode: "view",
      });
      setList(addList);
    },
    edit: (nameIndex: number, updatedName: { name: string }) => {
      //for editing element

      const editedElement: todo = list.map((item, i) => {
        if (i === nameIndex) {
          return {
            ...updatedName,
            mode: "view",
          };
        }
        return item;
      });
      console.log("editis called", editedElement);
      setList(editedElement);
    },

    ListEdit: (listIndex: number) => {
      const newList: todo = list.map((item, i) => {
        // => ({}) [{}]--error
        if (listIndex == i) {
          return {
            ...item,

            mode: "edit",
          };
        } else {
          return item;
        }
      });

      //console.log(newList)
      setList(newList);
    },

    delete: (itemIndex: number) => {
      // for deleting element
      const updatedList: todo = list.filter(
        (val, index) => index !== itemIndex
      );
      setList(updatedList);
    },
  };

  return (
    <div>
      <div>
        <h1>Add a new user</h1>
        <EditorContext.Provider value={{ type: "add", onAdd: _fn.add }}>
          <Editor />
        </EditorContext.Provider>
      </div>
      <div>
        {list.map((item, i) => {
          return (
            <div key={i}>
              {item.mode == "edit" ? (
                <EditorContext.Provider
                  value={{
                    type: "edit",
                    index: i,
                    name: item.name,
                    onEdit: _fn.edit,
                  }}
                >
                  <Editor />
                </EditorContext.Provider>
              ) : (
                <CardContext.Provider
                  value={{
                    item: item,
                    index: i,
                    onDelete: _fn.delete,
                    ListEdit: _fn.ListEdit,
                  }}
                >
                  <Card />
                </CardContext.Provider>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default Index;
