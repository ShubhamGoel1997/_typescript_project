import React, { useContext } from "react";
import { CardContext } from "./index";

type cardtype = {
  item?: { name: string };
  index?: number;
  onDelete?: (index: number) => void;
  ListEdit?: (index: number) => void;
};

const Card: React.FC<cardtype> = () => {
  const UserCardContext = useContext(CardContext);

  return (
    <div>
      {/* {display element} */ UserCardContext.item?.name}
      {
        /* {Make a delete button} */
        <div>
          <button
            onClick={() => {
              UserCardContext.onDelete?.(UserCardContext.index!);
            }}
          >
            Delete
          </button>
          <button
            onClick={() => {
              UserCardContext.ListEdit?.(UserCardContext.index!);
            }}
          >
            edit
          </button>
        </div>
      }
    </div>
  );
};
export default Card;
