import react, { useContext, useState } from "react";
import { EditorContext } from "./index";

type editortype = {
  name?: string;
  type?: string;
  index?: number;
  onEdit?: (index: number, obj: { name: string }) => void;
  onAdd?: (obj: { name: string }) => void;
};

const Editor: React.FC = () => {
  const UserEditorContext = useContext<editortype>(EditorContext);
  const [state, setState] = useState({
    name: UserEditorContext.name == undefined ? "" : UserEditorContext.name,
  });

  const _fn1 = {
    save: () => {
      //For editing and adding
      //if type='edit' call onEdit
      //if type='add' call onAdd

      if (UserEditorContext.type == "edit") {
        UserEditorContext.onEdit?.(UserEditorContext.index!, state);
      } else {
        UserEditorContext.onAdd?.(state);
      }
    },
  };

  return (
    <div>
      {/* {display type Add or Edit} */ UserEditorContext.type}
      <input
        value={state.name}
        onChange={(e) => {
          setState({ ...state, name: e.target.value });
        }}
      />

      {
        /* {make a button to save} */
        <button onClick={_fn1.save}>Save data</button>
      }
    </div>
  );
};
export default Editor;
