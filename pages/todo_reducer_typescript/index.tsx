import React, { useState, useReducer } from "react";
import Editor from "./Editor";
import Card from "./Card";

type ACTIONS =
  | { type: "add"; payload: { name: string } }
  | { type: "edit"; payload: { name: string; index: number } }
  | { type: "switch2EditMode"; payload: { index: number } }
  | { type: "deleteItem"; payload: { index: number } };

type initialData = {
  name: string;
  mode: "edit" | "view";
}[];

const reducer = (list: initialData, action: ACTIONS): initialData => {
  switch (action.type) {
    case "add":
      const addObj = [...list];
      addObj.push({
        ...action.payload,
        mode: "view",
      });
      console.log(addObj);
      return addObj;

    case "edit":
      const editElement: initialData = list.map((value, i) => {
        if (i === action.payload.index) {
          return {
            ...action.payload,
            mode: "view",
          };
        }
        return value;
      });
      return editElement;

    case "switch2EditMode":
      const newList: initialData = list.map((value, i) => {
        return {
          ...value,
          mode: action.payload.index == i ? "edit" : "view",
        };
      });
      return newList;

    case "deleteItem":
      const filteredList = list.filter(
        (value, i) => i !== action.payload.index
      );
      return filteredList;

    default:
      return list;
  }
};

const index: React.FC = () => {
  const [objData, dispatch] = useReducer(reducer, []);

  const _fn1 = {
    forAdd: (data: { name: string }) => {
      dispatch({ type: "add", payload: { name: data.name } });
    },
    forEdit: (index: number, data: { name: string }) => {
      dispatch({ type: "edit", payload: { name: data.name, index: index } });
    },
    forDelete: (index: number) => {
      dispatch({ type: "deleteItem", payload: { index: index } });
    },
    forSwitch2Edit: (index: number) => {
      dispatch({ type: "switch2EditMode", payload: { index: index } });
    },
  };

  return (
    <div>
      <div>
        <h1>Add Avenger</h1>
        <Editor type="add" dispatchAdd={_fn1.forAdd} />
      </div>
      <div>
        {objData.map((item, i) => {
          return (
            <div key={i}>
              {item.mode == "edit" ? (
                <Editor
                  type="edit"
                  index={i}
                  {...item}
                  name={item.name}
                  dispatchEdit={_fn1.forEdit}
                />
              ) : (
                <Card
                  item={item}
                  index={i}
                  dispatchDelete={_fn1.forDelete}
                  dispatchSwitch2EditMode={_fn1.forSwitch2Edit}
                />
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default index;
