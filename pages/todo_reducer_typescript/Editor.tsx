import React, { useReducer } from "react";

type EditorPropType = {
  name?: string;
  type?: string;
  index?: number;
  dispatchEdit?: (index: number, obj: { name: string }) => void;
  dispatchAdd?: (obj: { name: string }) => void;
};

const Editor: React.FC<EditorPropType> = (props) => {
  type State = {
    name: string;
  };

  type Actions = { type: "setState"; payload: { name: string } };

  const reducer = (state: State, action: Actions) => {
    switch (action.type) {
      case "setState":
        return { ...state, name: action.payload.name };
    }
  };

  const initialState = { name: props.name == undefined ? "" : props.name };

  const [state, dispatch] = useReducer(reducer, initialState);

  const _fn = {
    save: () => {
      if (props.type == "edit") {
        props.dispatchEdit?.(props.index!, state);
      } else {
        props.dispatchAdd?.(state);
      }
    },
    onSet: (e: React.ChangeEvent<HTMLInputElement>) => {
      dispatch({ type: "setState", payload: { name: e.target.value } });
    },
  };
  return (
    <div>
      {props.type}

      <input value={state.name} onChange={_fn.onSet} />

      {<button onClick={_fn.save}>Save</button>}
    </div>
  );
};

export default Editor;
