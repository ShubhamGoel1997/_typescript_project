import React from "react";

type CardPropType = {
  item?: { name: string };
  index?: number;
  dispatchDelete?: (index: number) => void;
  dispatchSwitch2EditMode?: (index: number) => void;
};

const Card: React.FC<CardPropType> = (props) => {
  return (
    <div>
      {/* {display element} */ props.item?.name}
      {
        /* {Make a delete button}
            {Make a edit button}
       */
        <div>
          <button
            onClick={() => {
              props.dispatchDelete?.(props.index!);
            }}
          >
            Delete
          </button>
          <button
            onClick={() => {
              props.dispatchSwitch2EditMode?.(props.index!);
            }}
          >
            edit
          </button>
        </div>
      }
    </div>
  );
};

export default Card;
