import React, { useState } from "react";
import Dropdown, { apiData } from "./Dropdown";

export const DropdownComponantContext = React.createContext(null as any);

const index: React.FC = () => {
  const [dataOptions, setDataOptions] = useState<apiData>([]);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [input, setInput] = useState<string>("");
  const [selectedOption, setSelectedOption] = useState<null | string | apiData>(
    null
  );

  return (
    <div>
      <DropdownComponantContext.Provider
        value={{
          isOpen,
          selectedOption,
          input,
          dataOptions,
          setDataOptions,
          setInput,
          setIsOpen,
          setSelectedOption,
        }}
      >
        <Dropdown />
      </DropdownComponantContext.Provider>
    </div>
  );
};

export default index;
