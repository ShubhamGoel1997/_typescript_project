import { GetStaticPaths, GetStaticProps, InferGetStaticPropsType } from "next";
import React from "react";

type dataType = {
  id: number;
  userId: number;
  title: string;
  body: string;
};

type propType = {
  data: dataType;
};

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data: dataType[] = await res.json();

  const paths = data.map((item, index) => {
    return {
      params: {
        id: item.id.toString(),
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<propType> = async (context) => {
  const id = context.params?.id;

  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};
export const Posts = (
  props: InferGetStaticPropsType<typeof getStaticProps>
) => {
  return (
    <div>
      {props.data.id} <br />
      {props.data.title} <br />
      {props.data.body}
    </div>
  );
};

export default Posts;
