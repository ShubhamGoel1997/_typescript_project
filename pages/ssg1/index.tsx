import { GetStaticProps, InferGetStaticPropsType } from "next";
import React from "react";

type dataType = {
  id: number;
  title: string;
}[];

type propType = {
  data: dataType;
};

export const getStaticProps: GetStaticProps<propType> = async () => {
  const res = await fetch("https://jsonplaceholder.typicode.com/posts");
  const data = await res.json();

  return {
    props: {
      data,
    },
  };
};

const Index = (props: InferGetStaticPropsType<typeof getStaticProps>) => {
  return (
    <div>
      {props.data.map((item, index) => (
        <div key={index}>{item.id}</div>
      ))}
    </div>
  );
};

export default Index;
